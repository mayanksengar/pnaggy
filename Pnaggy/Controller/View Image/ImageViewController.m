//
//  ImageViewController.m
//  Pnaggy
//
//  Created by Mayank Sengar on 2/5/18.
//  Copyright © 2018 Mayank Sengar. All rights reserved.
//

#import "ImageViewController.h"

#define ZOOM_STEP 2.0


@interface ImageViewController ()
{
    //NSArray * imageArray;
    UIImageView *imageView;
    UIImageView *downImageView;
    UIView *backGroundView;
    UIImage *aspectedImage;
    
    int x;
    
}


@end

@implementation ImageViewController

#pragma mark - View Delegate Functions
- (void)viewDidLoad {
    [super viewDidLoad];
    
    x = 0 ;
    [self.view setBackgroundColor:[UIColor blackColor]];
    
    NSLog(@"Images : %@",self.imageArr);
    NSLog(@"Index : %d",self.index);
    
    [self designOfViewImage];
    [self customDesignChages];
 
}

-(void)viewWillAppear:(BOOL)animated
{
    [_scroller setContentOffset:CGPointMake(self.view.frame.size.width*self.index, 0) animated:YES];

    
    if (self.index >= 0 )
    {
        [backGroundView setFrame:CGRectMake(backGroundView.frame.size.width*self.index, backGroundView.frame.origin.y, backGroundView.frame.size.width, backGroundView.frame.size.height)];
        
    }
}

#pragma mark - Designing Methods
-(void)customDesignChages
{
    [self.navigationBar setHidden:NO];
    [self.navigationBar setBackgroundColor:[UIColor clearColor]];
    [self.backArrowBtn setHidden:NO];
    [self.backBtn setHidden:NO];
    [self.saveButton setHidden:YES];
    //[self.backBtn setTintColor:mAppThemeColor];
    [self.backBtn setTitleColor:mAppThemeColor forState:UIControlStateNormal];
}

-(void)designOfViewImage
{    
    [_scroller setContentSize:CGSizeMake(_scroller.frame.size.width*self.imageArr.count, 0)];
    [_scroller setDelegate:(id)self];
    [_scroller setShowsHorizontalScrollIndicator:NO];
    [_scroller setPagingEnabled:YES];
    [_scroller setBouncesZoom:YES];
    [_scroller setClipsToBounds:YES];
    
    _scroller.decelerationRate = UIScrollViewDecelerationRateFast;

    
    
    [_downScroller setContentSize:CGSizeMake(50*self.imageArr.count, 0)];
    [_downScroller setDelegate:(id)self];
    [_downScroller setBounces:NO];
    
    
    for (int i =0; i<self.imageArr.count; i++)
    {
        
        aspectedImage = [self imageWithImage:[self.imageArr objectAtIndex:i] scaledToWidth:self.view.frame.size.width];

        [_scroller setFrame:CGRectMake(0, self.scroller.frame.origin.y, self.view.frame.size.width, aspectedImage.size.height)];

        
        imageView = [UIImageView new];
        [imageView setFrame:CGRectMake(0+(i*_scroller.frame.size.width), 0, self.scroller.frame.size.width, aspectedImage.size.height)];
        [imageView setImage:aspectedImage];
        [imageView setUserInteractionEnabled:YES];
        [imageView setTag:100+i];
        

        [_scroller addSubview:imageView];
        

        downImageView = [UIImageView new];
        [downImageView setFrame:CGRectMake(0+(i*50), 0, 50, _downScroller.frame.size.height)];
        [downImageView setImage:aspectedImage];
        [downImageView setUserInteractionEnabled:YES];
        [downImageView setTag:500+i];
        [_downScroller addSubview:downImageView];
        //
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImage:)];
        [tapGesture setNumberOfTapsRequired:1];
        [downImageView addGestureRecognizer:tapGesture];
    }
    
    aspectedImage =[_imageArr objectAtIndex:0 ];
    if (aspectedImage.size.height < 300)
    {
        [_scroller setFrame:CGRectMake(0, self.scroller.frame.origin.y + 100, self.view.frame.size.width, aspectedImage.size.height)];
    }
    else
    {
        [_scroller setFrame:CGRectMake(0, self.scroller.frame.origin.y, self.view.frame.size.width, aspectedImage.size.height)];
    }

    backGroundView = [UIView new];
    [backGroundView setFrame:CGRectMake(0, 0, downImageView.frame.size.width, downImageView.frame.size.height)];
    [backGroundView.layer setBorderWidth:3.0];
    [_downScroller addSubview:backGroundView];
    [self.view bringSubviewToFront:backGroundView];
    [backGroundView.layer setBorderColor:(mAppThemeColor) .CGColor];
}

-(void)tapImage:(UITapGestureRecognizer *)gesture
{
    NSLog(@"tag : %d",gesture.view.tag);
    
    int scrollWidth =  (int)(gesture.view.tag-500);

    aspectedImage =[_imageArr objectAtIndex:scrollWidth ];

//    if (aspectedImage.size.height < 300)
//    {
//        [_scroller setFrame:CGRectMake(100 +_scroller.frame.origin.x, _scroller.frame.origin.y, _scroller.frame.size.width, aspectedImage.size.height)];
//    }
//    else
//    {
        [_scroller setFrame:CGRectMake(_scroller.frame.origin.x, _scroller.frame.origin.y, _scroller.frame.size.width, aspectedImage.size.height)];
    //}
    
    [_scroller setContentOffset:CGPointMake(self.view.frame.size.width*scrollWidth, 0) animated:YES];
    
    [backGroundView setFrame:CGRectMake(backGroundView.frame.size.width*scrollWidth, backGroundView.frame.origin.y, backGroundView.frame.size.width, backGroundView.frame.size.height)];
    
}


#pragma mark - Function: Aspect Ration Of Image
-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - ScrollView Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int scrollerWidth  = _scroller.contentOffset.x;
    
    int x= scrollerWidth / (IS_iPhone_5?320:IS_iPhone_6?375:IS_iPhone_6P?414:375);

    if (x >= 0 )
    {
        aspectedImage =[_imageArr objectAtIndex:x ];
        [_scroller setFrame:CGRectMake(_scroller.frame.origin.x, _scroller.frame.origin.y, _scroller.frame.size.width, aspectedImage.size.height)];
        [backGroundView setFrame:CGRectMake(backGroundView.frame.size.width*x, backGroundView.frame.origin.y, backGroundView.frame.size.width, backGroundView.frame.size.height)];
    }
    
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return imageView;
}


- (void)scrollViewDidZoom:(UIScrollView *)aScrollView
{
    CGFloat offsetX = (_scroller.bounds.size.width > _scroller.contentSize.width)?
    (_scroller.bounds.size.width - _scroller.contentSize.width) * 0.5 : 0.0;
    
    CGFloat offsetY = (_scroller.bounds.size.height > _scroller.contentSize.height)?
    (_scroller.bounds.size.height - _scroller.contentSize.height) * 0.5 : 0.0;
    
    imageView.center = CGPointMake(_scroller.contentSize.width * 0.5 + offsetX,
                                   _scroller.contentSize.height * 0.5 + offsetY);
}

@end
