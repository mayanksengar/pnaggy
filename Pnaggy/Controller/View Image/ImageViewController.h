//
//  ImageViewController.h
//  Pnaggy
//
//  Created by Mayank Sengar on 2/5/18.
//  Copyright © 2018 Mayank Sengar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : CommonViewController
@property (strong, nonatomic) IBOutlet UIScrollView *scroller;
@property (strong, nonatomic) IBOutlet UIScrollView *downScroller;

@property (nonatomic, strong)NSArray * imageArr;
@property(nonatomic) int index;


@property (nonatomic, strong)AKImageViewerViewController * imageViewController;


- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center;


@end
