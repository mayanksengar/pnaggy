//
//  CameraGalleryViewController.m
//  Pnaggy
//
//  Created by Mayank Sengar on 1/29/18.
//  Copyright © 2018 Mayank Sengar. All rights reserved.
//

#import "CameraGalleryViewController.h"
#import "ImageViewController.h"
#import "ImageListViewController.h"


#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>


@interface CameraGalleryViewController () <UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIImagePickerController *picker;
    NSArray *ImageArray;
    NSArray *centreImageViewArray;
    NSArray *textArray;
    UIImageView *imageView;
    
    NSTimer *timer;
    
    __block PHFetchResult *photosAsset;
    __block PHAssetCollection *collection;
    __block PHObjectPlaceholder *placeholder;
    
    float h;
}

@property (strong, nonatomic) LGPlusButtonsView *plusButtonsViewMain;

@end

@implementation CameraGalleryViewController

#pragma mark - View Delegate Function
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
     h = 0;
    
    ImageArray = [[NSArray alloc]initWithObjects:@"bg",@"bgg",@"bggg", nil];
    centreImageViewArray= [[NSArray alloc] initWithObjects:@"camera",@"gallery",@"icon_pnagy", nil];
    textArray =[[NSArray alloc]initWithObjects:mCameraString,mGalleryString,mMyDMCString, nil];
    
    [self.navigationController.navigationBar setHidden:YES];
    
    
    [self checkCustomCreatedAlbumName];
    [self designOfCameraModule];
    
    [self designForLogo];
    
    
    
    UIButton *button = [self addButtonWithFrame:CGRectMake(self.view.frame.size.width - 60, 30, 30, 30) bgColor:[UIColor clearColor] image:[UIImage imageNamed:@"share"] tag:1212];
    
    [self.view addSubview:button];
    [self.view bringSubviewToFront:button];
    [self addFloatingButton];
     
    
}


-(void)viewDidDisappear:(BOOL)animated
{
    [timer invalidate];
}
-(void)viewWillAppear:(BOOL)animated
{
    
   // timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
}


#pragma mark - Designs
-(void)designOfCameraModule
{
    UIImage * cameraImg = [UIImage imageNamed:@"camera"];
    UIImage * whiteScrollerImg = [UIImage imageNamed:IS_iPhone_5?@"semiiii":@"SemiCircle"];

    [self.scrollView setFrame:CGRectMake(0,0, self.view.frame.size.width, 350)];
    [self.scrollView setDelegate:(id)(self)];
    [self.scrollView setPagingEnabled:YES];
    [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width*ImageArray.count,0)];
    [self.scrollView setBounces:NO];
    
    [self.whiteCurveView setFrame:CGRectMake(0, self.whiteCurveView.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height/2+20)];
    [self.whiteCurveView setBackgroundColor:[UIColor colorWithPatternImage:whiteScrollerImg]];
    [self.whiteScrollView setPagingEnabled:YES];
    [self.whiteScrollView setDelegate:(id)(self)];
    [self.whiteScrollView setContentSize:CGSizeMake(self.view.frame.size.width*ImageArray.count,0)];

    CGFloat x=0;
    CGFloat x_origin=self.view.frame.size.width/2-cameraImg.size.width/2;
    CGFloat xOriginForLable = (IS_iPhone_5?20:IS_iPhone_6?25:IS_iPhone_X?25:30);
    CGFloat yOriginForLable = (IS_iPhone_5?50:IS_iPhone_6?70:IS_iPhone_X?75:80);
    
    for(int i=0;i<ImageArray.count;i++)
    {
        UIImageView *upperScrollerImageView=[[UIImageView alloc]initWithFrame:CGRectMake(x+(self.view.frame.size.width*i), 0, self.view.frame.size.width, self.scrollView.frame.size.height)];
        [upperScrollerImageView setImage:[UIImage imageNamed:[ImageArray objectAtIndex:i]]];
        [self.scrollView addSubview:upperScrollerImageView];

        //UIImageView *lowerScrollerImageView=[[UIImageView alloc]initWithFrame:CGRectMake(x_origin+(self.view.frame.size.width*i), 50, cameraImg.size.width, cameraImg.size.height)];
        UIImageView *lowerScrollerImageView=[[UIImageView alloc]initWithFrame:CGRectMake(x_origin+(self.view.frame.size.width*i), 50, cameraImg.size.width, cameraImg.size.height)];
        lowerScrollerImageView.tag=50+i;
        [lowerScrollerImageView setImage:[UIImage imageNamed:[centreImageViewArray objectAtIndex:i]]];
        [self.whiteScrollView addSubview:lowerScrollerImageView];
        
        

        UILabel*label=[[UILabel alloc]initWithFrame:CGRectMake(xOriginForLable +(self.view.frame.size.width*i), lowerScrollerImageView.frame.origin.y+lowerScrollerImageView.frame.size.height+yOriginForLable, self.view.frame.size.width - (IS_iPhone_5?40:IS_iPhone_6?50:IS_iPhone_X?50:60 ),IS_iPhone_5?110:IS_iPhone_6?85:IS_iPhone_X?90:95)];
        label.text=[textArray objectAtIndex:i];
        label.textAlignment=NSTextAlignmentCenter;
        label.textColor=mAppThemeColor;
        [label setNumberOfLines:0];
        [self.whiteScrollView addSubview:label];
        
        
        /*
         *  Scroller Animation
         */
//        if(i==0)
//        {
//          [self setAnimation:lowerScrollerImageView];
//        }

    }
}

-(void)designForLogo
{

    UILabel *eSurgiLogo = [[UILabel alloc]initWithFrame:CGRectMake(IS_iPhone_5?15:IS_iPhone_6?20:IS_iPhone_X?20:25, 30, 150, 40)];
    [self.view addSubview:eSurgiLogo];
    [eSurgiLogo setText:Esurgi];
    [eSurgiLogo setTextColor:mAppThemeColor];
    [eSurgiLogo setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size: IS_iPhone_5?23:IS_iPhone_6?25:IS_iPhone_X?25:27]];
    
//    UIImageView *eSurgiLogoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(20,80, 70, 70)];
//    [eSurgiLogoImageView setImage:[UIImage imageNamed:@"icon_pnagy"]];
//    [self.view addSubview:eSurgiLogoImageView];
//    
    
}

#pragma mark - ScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(scrollView == self.scrollView)
    {
        if(self.scrollView.contentOffset.x==0)
        {
            [self.scrollView setContentOffset:CGPointMake(0,0) animated:YES ];
            [self.whiteScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        }
        else if(self.scrollView.contentOffset.x== self.view.frame.size.width)
        {
            [self.scrollView setContentOffset:CGPointMake(self.view.frame.size.width,0) animated:YES ];
            [self.whiteScrollView setContentOffset:CGPointMake(self.view.frame.size.width, 0) animated:YES];
        }
        else
        {
            [self.scrollView setContentOffset:CGPointMake(self.view.frame.size.width*2,0) animated:YES ];
            [self.whiteScrollView setContentOffset:CGPointMake(self.view.frame.size.width*2, 0) animated:YES];
        }
    }
    else if(scrollView == self.whiteScrollView)
    {
        if(self.whiteScrollView.contentOffset.x==0)
        {
//            UIImageView*imgViewww=(UIImageView *)[_whiteScrollView viewWithTag:50];
//            [self setAnimation:imgViewww];
            [self.scrollView setContentOffset:CGPointMake(0,0) animated:YES ];
            [self.whiteScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        }
        else if(self.whiteScrollView.contentOffset.x== self.view.frame.size.width)
        {
//            UIImageView*imgViewww=(UIImageView *)[_whiteScrollView viewWithTag:51];
//            [self setAnimation:imgViewww];
            [self.scrollView setContentOffset:CGPointMake(self.view.frame.size.width,0) animated:YES ];
            [self.whiteScrollView setContentOffset:CGPointMake(self.view.frame.size.width, 0) animated:YES];
        }
        else
        {
//            UIImageView*imgViewww=(UIImageView *)[_whiteScrollView viewWithTag:52];
//            [self setAnimation:imgViewww];
            [self.scrollView setContentOffset:CGPointMake(self.view.frame.size.width*2,0) animated:YES ];
            [self.whiteScrollView setContentOffset:CGPointMake(self.view.frame.size.width*2, 0) animated:YES];
        }
    }
}

-(void)setAnimation :(UIImageView*)imgView
{
    [UIView animateWithDuration:0.8
                     animations:^{
                         
                        imgView.frame = CGRectOffset(imgView.frame,-(self.view.frame.size.width/2+imgView.frame.size.width/2), 0);
                     }
                     completion:^(BOOL finished) {
                         
                         // block fires when animation has finished
                     }];
}



- (void) onTimer {
    
    // Updates the variable h, adding 100 (put your own value here!)
    
    NSLog(@"The Value of the  h:%f ",h);
  
    [self.scrollView setContentOffset:CGPointMake(h,0) ];
    [self.whiteScrollView setContentOffset:CGPointMake(h, 0)];
    
    h += self.view.frame.size.width;
    float j =(ImageArray.count*self.view.frame.size.width);
    int k = h/(self.view.frame.size.width);
    
    switch (k) {
        case 1:
            [self.scrollerImageView setImage:[UIImage imageNamed:[ImageArray objectAtIndex:0]]];
            [self.centerImageView setImage:[UIImage imageNamed:[centreImageViewArray objectAtIndex:0]]];
            [self.WhiteScrollViewLable setText:[textArray objectAtIndex:0]];
            break;
        case 2:
            [self.scrollerImageView setImage:[UIImage imageNamed:[ImageArray objectAtIndex:1]]];
            [self.centerImageView setImage:[UIImage imageNamed:[centreImageViewArray objectAtIndex:1]]];
            [self.WhiteScrollViewLable setText:[textArray objectAtIndex:1]];

            break;
        case 3:
            [self.scrollerImageView setImage:[UIImage imageNamed:[ImageArray objectAtIndex:2]]];
            [self.centerImageView setImage:[UIImage imageNamed:[centreImageViewArray objectAtIndex:2]]];
            [self.WhiteScrollViewLable setText:[textArray objectAtIndex:2]];

            break;
            
        default:
            break;
    }
    
    if (h > j -1)
    {
        h = 0;
    }
}


#pragma mark - Touch Event
- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    [timer invalidate];
}




/*
 *  Button Action Methods
 */
#pragma mark - Sharing Button Action
-(IBAction)buttonAction:(UIButton *)sender
{
    if (sender.tag == 1212)
    {
        
        NSString * message = mSharingText;
        
        UIImage * image = [UIImage imageNamed:@"icon_pnagy"];
        
        NSArray * shareItems = @[message, image];
        
        UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
        
        [self presentViewController:avc animated:YES completion:nil];
        
    }
    
}



#pragma mark - Camera and Gallery Image Picker
- (void)goToCamera
{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized)
    {
        if (TARGET_OS_SIMULATOR)
        {
            showAlert(@"You have not allowed this feature on simulator ", @"Info");
        }
        else
        {
            UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
            imagePickerController.delegate = (id)self;
            imagePickerController.sourceType =  UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:imagePickerController animated:YES completion:nil];
            
        }
        
    }
    else if(authStatus == AVAuthorizationStatusNotDetermined)
    {
        NSLog(@"%@", @"Camera access not determined. Ask for permission.");
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
         {
             if(granted)
             {
                 NSLog(@"Granted access to %@", AVMediaTypeVideo);
                 if (TARGET_OS_SIMULATOR)
                 {
                     showAlert(@"You have not allowed this feature on simulator ", @"Info");
                 }
                 else
                 {
                     UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
                     imagePickerController.delegate = (id)self;
                     imagePickerController.sourceType =  UIImagePickerControllerSourceTypeCamera;
                     [self presentViewController:imagePickerController animated:YES completion:nil];
                 }
             }
             else
             {
                 NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                 [self camDenied];
             }
         }];
    }
    else if (authStatus == AVAuthorizationStatusRestricted)
    {
         showAlert(@"You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access.", @"Info");
       
    }
    else
    {
        [self camDenied];
    }
}

- (void)camDenied
{
    NSLog(@"%@", @"Denied camera access");
    NSString *alertText;
    NSString *alertButton;
    
    BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
    if (canOpenSettings)
    {
        alertText = @"It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Touch the Go button below to open the Settings app.\n\n2. Touch Privacy.\n\n3. Turn the Camera on.\n\n4. Open this app and try again.";
        
        alertButton = @"Go";
    }
    else
    {
        alertText = @"It looks like your privacy settings are preventing us from accessing your camera to do barcode scanning. You can fix this by doing the following:\n\n1. Close this app.\n\n2. Open the Settings app.\n\n3. Scroll to the bottom and select this app in the list.\n\n4. Touch Privacy.\n\n5. Turn the Camera on.\n\n6. Open this app and try again.";
        
        alertButton = @"OK";
    }
        
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Error"
                          message:alertText
                          delegate:self
                          cancelButtonTitle:alertButton
                          otherButtonTitles:nil];
    alert.tag = 3491832;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 3491832)
    {
        BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
        if (canOpenSettings)
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
}

#pragma mark - UIImagePickerViewController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(nullable NSDictionary<NSString *,id> *)editingInfo
{
    NSLog(@"The Image is captured : ");
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    [picker dismissViewControllerAnimated:true completion:nil];
    NSLog(@"The UIIMgePickerViewCOntroller didFinishPickingMediaWithInfo : %@",info);
    UIImage *imageeee = [info objectForKey:UIImagePickerControllerOriginalImage];
    [self checkCustomCreatedAlbumName];
    [self saveImageWithAlbumNameAndImage:imageeee];
    
    showAlert(@"Image is saved successfully", @"Alert");
//
//    ImageListViewController *imageee = [self.storyboard instantiateViewControllerWithIdentifier:@"idImageListVC"];
//        [self.navigationController pushViewController:imageee animated:YES];
    

}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    NSLog(@"USer is Tapped Cancel Button");
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}


/*
 * Create Album, Check if the album exists, Save in Album...
 */
#pragma mark - Create Custom Album name

-(void)checkCustomCreatedAlbumName
{
    
    // Find the album
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    fetchOptions.predicate = [NSPredicate predicateWithFormat:@"title = %@", MKCustomPhotoAlbumName];
    collection = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum
                                                          subtype:PHAssetCollectionSubtypeAny
                                                          options:fetchOptions].firstObject;
    
    // Create the album
    if (!collection)
    {
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            PHAssetCollectionChangeRequest *createAlbum = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:MKCustomPhotoAlbumName];
            
            placeholder = [createAlbum placeholderForCreatedAssetCollection];
        } completionHandler:^(BOOL success, NSError *error) {
            if (success)
            {
                PHFetchResult *collectionFetchResult = [PHAssetCollection fetchAssetCollectionsWithLocalIdentifiers:@[placeholder.localIdentifier]
                                                                                                            options:nil];
                collection = collectionFetchResult.firstObject;
            }
        }];
    }
}

-(void)saveImageWithAlbumNameAndImage:(UIImage *)image
{
    
    // Save to the album
    [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
        PHAssetChangeRequest *assetRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:image];
        
        placeholder = [assetRequest placeholderForCreatedAsset];
        
        photosAsset = [PHAsset fetchAssetsInAssetCollection:collection options:nil];
        PHAssetCollectionChangeRequest *albumChangeRequest = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:collection
                                                                                                                      assets:photosAsset];
        [albumChangeRequest addAssets:@[placeholder]];
    } completionHandler:^(BOOL success, NSError *error) {
        if (success)
        {
            NSString *UUID = [placeholder.localIdentifier substringToIndex:36];
            NSLog(@"UUID : %@",UUID);
        }
        else
        {
            NSLog(@"%@", error);
        }
    }];
}

#pragma mark - UIView Designing Functions
-(UIButton *)addButtonWithFrame:(CGRect)frame bgColor:(UIColor *)bgColor image:(UIImage *)image tag:(NSInteger) tag
{
    UIButton *button = [[UIButton alloc]initWithFrame:frame];
    [button setBackgroundColor:bgColor];
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchDown];
    [button setTag:tag];
    
    return button;
    
}

#pragma mark - iCloud Avability Check
- (BOOL) isICloudAvailable
{
    // Make sure a correct Ubiquity Container Identifier is passed
    NSURL *ubiquityURL = [[NSFileManager defaultManager]
                          URLForUbiquityContainerIdentifier:@"ABCDEFGHI0.com.acme.MyApp"];
    return ubiquityURL ? YES : NO;
}

#pragma mark - Floating Point Button
-(void)addFloatingButton
{
    
    _plusButtonsViewMain = [LGPlusButtonsView plusButtonsViewWithNumberOfButtons:4
                                                         firstButtonIsPlusButton:YES
                                                                   showAfterInit:YES
                                                                   actionHandler:^(LGPlusButtonsView *plusButtonView, NSString *title, NSString *description, NSUInteger index)
                            {
                                NSLog(@"actionHandler | title: %@, description: %@, index: %lu", title, description, (long unsigned)index);
                                
                                if (index == 1)
                                {
                                    [self goToCamera];
                                    
                                    
                                    for (UIViewController *controller in self.navigationController.viewControllers)
                                    {
                                        if ([controller isKindOfClass:[CameraGalleryViewController class]])
                                        {
                                            [self.navigationController popToViewController:controller
                                                                                  animated:NO];                                            break;
                                        }
                                    }
                                    
                                    [plusButtonView hideButtonsAnimated:YES
                                                      completionHandler:nil];
                                    
                                }
                                else if (index ==2)
                                {
                                    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
                                    imagePickerController.delegate = (id)self;
                                    imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
                                    [self presentViewController:imagePickerController animated:YES completion:nil];
                                    
                                    
                                    for (UIViewController *controller in self.navigationController.viewControllers)
                                    {
                                        if ([controller isKindOfClass:[CameraGalleryViewController class]])
                                        {
                                            [self.navigationController popToViewController:controller
                                                                                  animated:NO];                                            break;
                                        }
                                    }
                                    
                                    
                                    
                                    [plusButtonView hideButtonsAnimated:YES
                                                      completionHandler:nil];
                                    
                                    
                                }
                                else if (index == 3)
                                {
                                    
                                    for (UIViewController *controller in self.navigationController.viewControllers)
                                    {
                                        if ([controller isKindOfClass:[CameraGalleryViewController class]])
                                        {
                                            [self.navigationController popToViewController:controller
                                                                                  animated:NO];                                            break;
                                        }
                                    }
                                    
                                    
                                    [plusButtonView hideButtonsAnimated:YES
                                                      completionHandler:nil];

                                    ImageListViewController *image = [ImageListViewController new];
                                    [self.navigationController pushViewController:image animated:YES];
                                    
                                }
                                else
                                {
                                    NSLog(@"Do Nothing");

                                }
                                    }];
    
    _plusButtonsViewMain.observedScrollView = self.scrollView;
    _plusButtonsViewMain.coverColor = [UIColor colorWithWhite:1.f alpha:0.7];
    _plusButtonsViewMain.position = LGPlusButtonsViewPositionBottomRight;
    _plusButtonsViewMain.plusButtonAnimationType = LGPlusButtonAnimationTypeRotate;
    
    [_plusButtonsViewMain setButtonsTitles:@[@"=", @"", @"", @""] forState:UIControlStateNormal];
    [_plusButtonsViewMain setDescriptionsTexts:@[@"", @"Take a photo", @"Choose from gallery", @"My DMC"]];
    [_plusButtonsViewMain setDescriptionsFont:[UIFont fontWithName:@"Helvetica Neue" size:18] forOrientation:LGPlusButtonsViewOrientationAll ];
    
    [_plusButtonsViewMain setDescriptionsTextColor:mAppThemeColor];
    [_plusButtonsViewMain setButtonsImages:@[[NSNull new], [UIImage imageNamed:@"floatingButtonCamera"], [UIImage imageNamed:@"floatingButtonGallery"], [UIImage imageNamed:@"floatingButtonMyDMC"]]
                                  forState:UIControlStateNormal
                            forOrientation:LGPlusButtonsViewOrientationAll];
    
    [_plusButtonsViewMain setButtonsAdjustsImageWhenHighlighted:NO];
    [_plusButtonsViewMain setButtonsBackgroundColor:[UIColor colorWithRed:0.f green:0.5 blue:1.f alpha:1.f] forState:UIControlStateNormal];
    [_plusButtonsViewMain setButtonsBackgroundColor:[UIColor colorWithRed:0.2 green:0.6 blue:1.f alpha:1.f] forState:UIControlStateHighlighted];
    [_plusButtonsViewMain setButtonsBackgroundColor:[UIColor colorWithRed:0.2 green:0.6 blue:1.f alpha:1.f] forState:UIControlStateHighlighted|UIControlStateSelected];
    [_plusButtonsViewMain setButtonsSize:CGSizeMake(44.f, 44.f) forOrientation:LGPlusButtonsViewOrientationAll];
    [_plusButtonsViewMain setButtonsLayerCornerRadius:44.f/2.f forOrientation:LGPlusButtonsViewOrientationAll];
    [_plusButtonsViewMain setButtonsTitleFont:[UIFont boldSystemFontOfSize:24.f] forOrientation:LGPlusButtonsViewOrientationAll];
    [_plusButtonsViewMain setButtonsLayerShadowColor:[UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1.f]];
    [_plusButtonsViewMain setButtonsLayerShadowOpacity:0.5];
    [_plusButtonsViewMain setButtonsLayerShadowRadius:3.f];
    [_plusButtonsViewMain setButtonsLayerShadowOffset:CGSizeMake(0.f, 2.f)];
    [_plusButtonsViewMain setButtonAtIndex:0 size:CGSizeMake(56.f, 56.f)
                            forOrientation:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? LGPlusButtonsViewOrientationPortrait : LGPlusButtonsViewOrientationAll)];
    [_plusButtonsViewMain setButtonAtIndex:0 layerCornerRadius:56.f/2.f
                            forOrientation:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? LGPlusButtonsViewOrientationPortrait : LGPlusButtonsViewOrientationAll)];
    [_plusButtonsViewMain setButtonAtIndex:0 titleFont:[UIFont systemFontOfSize:40.f]
                            forOrientation:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? LGPlusButtonsViewOrientationPortrait : LGPlusButtonsViewOrientationAll)];
    [_plusButtonsViewMain setButtonAtIndex:0 titleOffset:CGPointMake(0.f, -3.f) forOrientation:LGPlusButtonsViewOrientationAll];
    [_plusButtonsViewMain setButtonAtIndex:1 backgroundColor:[UIColor colorWithRed:1.f green:0.f blue:0.5 alpha:1.f] forState:UIControlStateNormal];
    [_plusButtonsViewMain setButtonAtIndex:1 backgroundColor:[UIColor colorWithRed:1.f green:0.2 blue:0.6 alpha:1.f] forState:UIControlStateHighlighted];
    [_plusButtonsViewMain setButtonAtIndex:2 backgroundColor:[UIColor colorWithRed:1.f green:0.5 blue:0.f alpha:1.f] forState:UIControlStateNormal];
    [_plusButtonsViewMain setButtonAtIndex:2 backgroundColor:[UIColor colorWithRed:1.f green:0.6 blue:0.2 alpha:1.f] forState:UIControlStateHighlighted];
    [_plusButtonsViewMain setButtonAtIndex:3 backgroundColor:[UIColor colorWithRed:0.f green:0.7 blue:0.f alpha:1.f] forState:UIControlStateNormal];
    [_plusButtonsViewMain setButtonAtIndex:3 backgroundColor:[UIColor colorWithRed:0.f green:0.8 blue:0.f alpha:1.f] forState:UIControlStateHighlighted];
    
    [_plusButtonsViewMain setDescriptionsBackgroundColor:[UIColor whiteColor]];
    [_plusButtonsViewMain setDescriptionsTextColor:[UIColor blackColor]];
    [_plusButtonsViewMain setDescriptionsLayerShadowColor:[UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1.f]];
    [_plusButtonsViewMain setDescriptionsLayerShadowOpacity:0.25];
    [_plusButtonsViewMain setDescriptionsLayerShadowRadius:1.f];
    [_plusButtonsViewMain setDescriptionsLayerShadowOffset:CGSizeMake(0.f, 1.f)];
    [_plusButtonsViewMain setDescriptionsLayerCornerRadius:6.f forOrientation:LGPlusButtonsViewOrientationAll];
    [_plusButtonsViewMain setDescriptionsContentEdgeInsets:UIEdgeInsetsMake(4.f, 8.f, 4.f, 8.f) forOrientation:LGPlusButtonsViewOrientationAll];
    
   // [_plusButtonsViewMain setButtonsBackgroundImage:[UIImage imageNamed:@"floatingButtonToggle"] forState:UIControlStateNormal];
    
    
    for (NSUInteger i=1; i<=3; i++)
        [_plusButtonsViewMain setButtonAtIndex:i offset:CGPointMake(-6.f, 0.f)
                                forOrientation:(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone ? LGPlusButtonsViewOrientationPortrait : LGPlusButtonsViewOrientationAll)];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        [_plusButtonsViewMain setButtonAtIndex:0 titleOffset:CGPointMake(0.f, -2.f) forOrientation:LGPlusButtonsViewOrientationLandscape];
        [_plusButtonsViewMain setButtonAtIndex:0 titleFont:[UIFont systemFontOfSize:32.f] forOrientation:LGPlusButtonsViewOrientationLandscape];
    }
    
    [self.navigationController.view addSubview:_plusButtonsViewMain];
}



#pragma mark - Utility Extension
/*
 *   Get Lable Height
 */
- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

@end
