//
//  CameraGalleryViewController.h
//  Pnaggy
//
//  Created by Mayank Sengar on 1/29/18.
//  Copyright © 2018 Mayank Sengar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CameraGalleryViewController : CommonViewController

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIImageView *scrollerImageView;



@property (strong, nonatomic) IBOutlet UIImageView *whiteImageView;

@property (strong, nonatomic) IBOutlet UIScrollView *whiteScrollView;
@property (strong, nonatomic) IBOutlet UIImageView *centerImageView;

@property (strong, nonatomic) IBOutlet UILabel *WhiteScrollViewLable;


@property (weak, nonatomic) IBOutlet UIView *whiteCurveView;



- (IBAction)myDMCButtonAction:(UIButton *)sender;


@end
