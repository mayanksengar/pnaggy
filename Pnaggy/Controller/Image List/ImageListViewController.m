//
//  ImageListViewController.m
//  Pnaggy
//
//  Created by Mayank Sengar on 2/9/18.
//  Copyright © 2018 Mayank Sengar. All rights reserved.
//

#import "ImageListViewController.h"
#import <Photos/Photos.h>
#import "ImageViewController.h"

@interface ImageListViewController ()
{
    NSMutableArray *imageArray;
    UIScrollView *scroller;

}
@property (strong, nonatomic) LGPlusButtonsView *plusButtonsViewMain;

@end

@implementation ImageListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    
    [self.navigationBar setHidden:NO];
    [self.saveButton setHidden:YES];
    [self.backBtn setHidden:NO];
    [self.titleLabel setText:@"My DMC"];
    [self.titleLabel setTextColor:[UIColor whiteColor]];
    [self.backArrowBtn setHidden:NO];
    [self.backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.saveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    
    
    [self getAllPicturesFromPhotoLibrary];
}
-(void)viewWillAppear:(BOOL)animated
{
    [_plusButtonsViewMain isHidden];
    
}

#pragma mark - Design of Image List
-(void)designForDirectoryView
{
    
    float topHorz = IS_iPhone_5?15:IS_iPhone_6?20.0:17.0;
    float topVert = IS_iPhone_6P?25.0:IS_iPhone_X?20.0:IS_iPhone_5?18:20.0;
    
    
    scroller = [[UIScrollView alloc]initWithFrame:CGRectMake(0,self.navigationBar.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-self.navigationBar.frame.size.height)];
    [scroller setContentSize:CGSizeMake(0, 1000)];
    [scroller setDelegate:(id)self];
    [scroller setShowsVerticalScrollIndicator:NO];
    [self.view addSubview:scroller];
    
    
    
    for(int i=0; i<imageArray.count; i++)
    {
        if((i%3) == 0 && i!=0)
        {
            topHorz = IS_iPhone_5?15:IS_iPhone_6?20.0:17.0;
            topVert = topVert + (IS_iPhone_5?100:IS_iPhone_6?120:IS_iPhone_X?116:130);
        }
        
        UIView * view = [self addViewWithFrame:IS_iPhone_5?CGRectMake(topHorz, topVert, 90,90):IS_iPhone_6?CGRectMake(topHorz, topVert, 110,110):IS_iPhone_X?CGRectMake(topHorz, topVert, 110,110):CGRectMake(topHorz, topVert, 120,120) bgColor:[UIColor yellowColor]];
        [view setTag:800+i];
        [view setUserInteractionEnabled:YES];
        [scroller addSubview:view];
        
        UITapGestureRecognizer * tapGest = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
        [tapGest setNumberOfTapsRequired:1];
        [view addGestureRecognizer:tapGest];
        
        // LastModified
        // Size
        
        UIImage * images = [imageArray objectAtIndex:i];
        
        UIImageView * imageView = [self addImageViewWithFrame:CGRectMake(0, 0, view.frame.size.width,view.frame.size.width) image:images];
        [view addSubview:imageView];
        
        topHorz = topHorz + (IS_iPhone_6P?130:IS_iPhone_X?115:IS_iPhone_5?100:113);
    }
    
}


-(IBAction)tapGestureAction:(UITapGestureRecognizer *)gesture
{
    NSLog(@"Tag : %ld",gesture.view.tag-800);
    
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ImageViewController * imageVC = [main instantiateViewControllerWithIdentifier:@"idImageViewController"];
    imageVC.imageArr=imageArray;
    imageVC.index= (int)gesture.view.tag-800;
    [self.navigationController pushViewController:imageVC animated:YES];
}

/*
 * Get Images from specific Album
 */
#pragma mark - Fetch Images From the Custom Album
-(void)getAllPicturesFromPhotoLibrary
{
    __block PHAssetCollection *collection;
    
    // Find the album
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    fetchOptions.predicate = [NSPredicate predicateWithFormat:@"title = %@", MKCustomPhotoAlbumName];
    collection = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum
                                                          subtype:PHAssetCollectionSubtypeAny
                                                          options:fetchOptions].firstObject;
    
    PHFetchResult *collectionResult = [PHAsset fetchAssetsInAssetCollection:collection options:nil];
    
    NSMutableArray *assets = [[NSMutableArray alloc] init];
    
    [collectionResult enumerateObjectsUsingBlock:^(PHAsset *asset, NSUInteger idx, BOOL * _Nonnull stop)
     {
         [assets addObject:asset];
         
     }];
    
    PHImageRequestOptions * requestOptions = [[PHImageRequestOptions alloc] init];
    //requestOptions.resizeMode   = PHImageRequestOptionsResizeModeExact;
    //requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    // this one is key
    //requestOptions.synchronous = true;
    
    
    PHImageManager *manager = [PHImageManager defaultManager];
    imageArray = [NSMutableArray arrayWithCapacity:[assets count]];
    
    // assets contains PHAsset objects.
    __block UIImage *ima;
    
    for (PHAsset *asset in assets) {
        // Do something with the asset
        
        [manager requestImageForAsset:asset
                           targetSize:PHImageManagerMaximumSize//CGSizeMake(300, 300)
                          contentMode:PHImageContentModeDefault
                              options:requestOptions
                        resultHandler:^void(UIImage *image, NSDictionary *info) {
                            
                            
                            ima = image;

                            [imageArray addObject:ima];

                            
                            if (assets.count==imageArray.count)
                            {
                                //[self.collectionView reloadData];
                                [self designForDirectoryView];
                            }
                        }];
    }
    
    NSLog(@"images %lu", (unsigned long)imageArray.count);
    
}



#pragma mark - Custom Designing Functions
-(UIView *)addViewWithFrame:(CGRect)Frame bgColor:(UIColor *)bgcolor
{
    UIView *view = [[UIView alloc]init];
    
    [view setFrame:Frame];
    [view setBackgroundColor:bgcolor];
    
    return view;
}

-(UIImageView *)addImageViewWithFrame:(CGRect)FRame image:(UIImage *)image
{
    UIImageView *imageView = [UIImageView new];
    [imageView setFrame:FRame];
    [imageView setImage:image];
    
    
    return imageView;
}


-(UILabel *)addLabelWithFrame:(CGRect)FRame text:(NSString *)text font:(UIFont *)font textColor:(UIColor *)textColor textAlign:(NSTextAlignment)textAlign
{
    UILabel *lable = [UILabel new];
    [lable setFrame:FRame];
    [lable setText:text];
    [lable setFont:font];
    [lable setTextColor:textColor];
    [lable setTextAlignment:textAlign];
    
    
    
    return lable;
}


@end
