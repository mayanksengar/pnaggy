//
//  AppConstants.h
//  Pnaggy
//
//  Created by Mayank Sengar on 1/31/18.
//  Copyright © 2018 Mayank Sengar. All rights reserved.
//

#ifndef AppConstants_h
#define AppConstants_h



#pragma mark - App Constants Strings

#define MKCustomPhotoAlbumName  @"My DMC"

#define Esurgi @"Esurgi, Inc"

#define mCameraString  @"You can use Camera to take a photo of a medicine to let that be saved in Pnaggy DMC and quickly retrieve that when needed."

#define mGalleryString @"Access a picture of your medicine which is already lying in your photo gallery and save the same in your DMC for quick retrieval when needed."

#define mMyDMCString @"Your Digital Medicine Cabinet is an easy and secure source for you to access photos of your medicines from Pnaggy app without hassle."


#define mSharingText   @"Drug-related errors cause an estimated 7000 deaths per year in the USA. Help your family, friends, colleagues and patient,  by inviting them to communicate more effectively to keep a current medication list. When you share this app with others, they can download it for their own use; in addition, we will share infrequent useful information with them."


/*
 *  App Constants
 */
#define mAppThemeColor [UIColor colorWithRed:63/255.0 green:158/255.0 blue:199/255.0 alpha:1.0]






/*
 *
 *   StoryBoard Id Naming
 */
#define loadViewController(StoryBoardName, VCIdentifer) [getStoryboard(StoryBoardName) instantiateViewControllerWithIdentifier:VCIdentifer]


#define     VC_CameraGallery                                 @"cameraViewController"
#define     VC_ImageCollection                               @"ImageCollectionVC"






#endif /* AppConstants_h */
