//
//  MONativeClass.m
//  modigy
//
//  Created by Chetan Rajauria on 1/27/14.
//  Copyright (c) 2014 Chetan Rajauria. All rights reserved.
//

#import "MONativeClass.h"

@implementation MONativeClass

void showAlert(NSString *messageString, NSString *titleString)
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:titleString message:messageString
                                                   delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alert show];
}

void showActivityView(NSString *text_label) {
    
    //[DejalKeyboardActivityView activityView];
}

void showActivityViewWithOwnView(NSString *text_label, UIView *view) {
    
    //[DejalKeyboardActivityView activityViewForView:view withLabel:text_label width:140];
}
void removeActivityView ()
{
    // Remove the activity view, with animation for the two styles that support it:
    //[DejalKeyboardActivityView removeViewAnimated:YES];
}

@end
