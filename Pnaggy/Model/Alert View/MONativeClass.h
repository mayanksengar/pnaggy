//
//  MONativeClass.h
//  modigy
//
//  Created by Narendra Kumar on 1/27/14.
//  Copyright (c) 2014 Narendra Kumar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MONativeClass : NSObject

void showAlert(NSString *messageString, NSString *titleString);

void showActivityView(NSString *text_label);

void showActivityViewWithOwnView(NSString *text_label ,UIView *view);

void removeActivityView ();

@end
