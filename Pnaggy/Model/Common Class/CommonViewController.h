//
//  CommonViewController.h
//  Pnaggy
//
//  Created by Mayank Sengar on 2/6/18.
//  Copyright © 2018 Mayank Sengar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonViewController : UIViewController

@property (nonatomic, strong)UIView * navigationBar;
@property (nonatomic, strong)UILabel * titleLabel;
@property (nonatomic, strong)UIButton * backArrowBtn;
@property (nonatomic, strong)UIButton * backBtn;


@property (nonatomic, strong)UIButton * saveButton;

//Progress Bar : MBProgress HUD
@property (atomic, assign) BOOL canceled;


-(void)startHUD;




@end
