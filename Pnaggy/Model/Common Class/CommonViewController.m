//
//  CommonViewController.m
//  Pnaggy
//
//  Created by Mayank Sengar on 2/6/18.
//  Copyright © 2018 Mayank Sengar. All rights reserved.
//

#import "CommonViewController.h"
//#import "Constants.h"
//#import <MBProgressHUD.h>

@interface CommonViewController ()
{

}

@end

@implementation CommonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    [self.navigationController.navigationBar setHidden:YES];
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    //Navigation Bar Controller
    [self designOfNavigationController];
    
    
}

-(void)designOfNavigationController
{
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"backgroundImage"]]];

    self.navigationBar = [[UIView alloc]init];
    [self.navigationBar setFrame:CGRectMake(0, 0, self.view.frame.size.width, IS_iPhone_4?65:IS_iPhone_5?65:IS_iPhone_6?70:75)];
    [self.navigationBar setBackgroundColor:mAppThemeColor];
    [self.navigationBar setHidden:YES];
    [self.view addSubview:self.navigationBar];
    
    
    
    
    self.titleLabel = [[UILabel alloc]init];
    [self.titleLabel setFrame:CGRectMake(50, 3, self.view.frame.size.width-100, self.navigationBar.frame.size.height)];
    [self.titleLabel setTextColor:[UIColor whiteColor]];
    [self.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:IS_iPhone_4?18:IS_iPhone_5?18:IS_iPhone_6?20:22]];
    [self.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.navigationBar addSubview:self.titleLabel];
    
    
    
    self.saveButton = [self addButtonWithFrame:CGRectMake(self.navigationBar.frame.size.width-80, self.navigationBar.frame.origin.y+self.navigationBar.frame.size.height/2-40/2+5, 80, 40) tag:451 image:nil title:nil color:[UIColor clearColor] font:[UIFont fontWithName:@"HelveticaNeue-Medium" size:IS_iPhone_4?18:IS_iPhone_5?18:IS_iPhone_6?20:22]];
    
    [self.saveButton setTitle:@"Save" forState:UIControlStateNormal];
    
    [self.saveButton setTitleColor:[UIColor colorWithRed:190/255.0 green:199/255.0 blue:204/255.0 alpha:1.0] forState:UIControlStateNormal];
    [self.navigationBar addSubview:self.saveButton];
    
    
    UIImage * backImage = [UIImage imageNamed:@"BackButton"];
    
    self.backArrowBtn = [self addButtonWithFrame:CGRectMake(10, 4, backImage.size.width+10, self.navigationBar.frame.size.height) tag:1111 image:backImage title:nil color:nil font:nil] ;
    [self.backArrowBtn addTarget:self action:@selector(sideMenuButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.navigationBar addSubview:self.backArrowBtn];
    
    self.backBtn = [self addButtonWithFrame:IS_iPhone_6P?CGRectMake(self.backArrowBtn.frame.origin.x+self.backArrowBtn.frame.size.width, self.backArrowBtn.frame.origin.y, 60, self.backArrowBtn.frame.size.height):IS_iPhone_6?CGRectMake(self.backArrowBtn.frame.origin.x+self.backArrowBtn.frame.size.width, self.backArrowBtn.frame.origin.y, 80, self.backArrowBtn.frame.size.height):CGRectMake(self.backArrowBtn.frame.origin.x+self.backArrowBtn.frame.size.width, self.backArrowBtn.frame.origin.y, 80, self.backArrowBtn.frame.size.height) tag:1112 image:nil title:@"Back" color:[UIColor clearColor] font:[UIFont fontWithName:@"HelveticaNeue-Medium" size:IS_iPhone_4?18:IS_iPhone_5?18:IS_iPhone_6?20:22]] ;
    [self.backBtn setTitleColor:[UIColor colorWithRed:190/255.0 green:199/255.0 blue:204/255.0 alpha:1.0] forState:UIControlStateNormal];
    [self.backBtn addTarget:self action:@selector(sideMenuButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.backBtn setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    [self.navigationBar addSubview:self.backBtn];
    
}


#pragma mark : - MBProgress HUD.
//
//-(void)startHUD
//{
//    [self networkingExample];
//}
//
//- (void)networkingExample {
//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//
//    // Set some text to show the initial status.
//    hud.label.text = NSLocalizedString(@"Preparing...", @"HUD preparing title");
//    // Will look best, if we set a minimum size.
//    hud.minSize = CGSizeMake(150.f, 100.f);
//
//    [self doSomeNetworkWorkWithProgress];
//}
//
//- (void)doSomeNetworkWorkWithProgress {
//    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:nil];
//    NSURL *URL = [NSURL URLWithString:@"https://support.apple.com/library/APPLE/APPLECARE_ALLGEOS/HT1425/sample_iPod.m4v.zip"];
//    NSURLSessionDownloadTask *task = [session downloadTaskWithURL:URL];
//    [task resume];
//}
//
//
//
//- (void)annularDeterminateExample {
//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//
//    // Set the annular determinate mode to show task progress.
//  //  hud.mode = MBProgressHUDModeAnnularDeterminate;
//    hud.mode = MBProgressHUDModeIndeterminate;
//
//    hud.label.text = NSLocalizedString(@"Loading", @"HUD loading title");
//
//    dispatch_async(dispatch_get_global_queue(QOS_CLASS_USER_INITIATED, 0), ^{
//        // Do something useful in the background and update the HUD periodically.
//        [self doSomeWorkWithProgress];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [hud hideAnimated:YES];
//        });
//    });
//}
//
//
//- (void)doSomeWorkWithProgress {
//    self.canceled = NO;
//    // This just increases the progress indicator in a loop.
//    float progress = 0.0f;
//    while (progress < 1.0f) {
//        if (self.canceled) break;
//        progress += 0.01f;
//        dispatch_async(dispatch_get_main_queue(), ^{
//            // Instead we could have also passed a reference to the HUD
//            // to the HUD to myProgressTask as a method parameter.
//            [MBProgressHUD HUDForView:self.navigationController.view].progress = progress;
//        });
//        usleep(50000);
//    }
//}
//




#pragma mark - Button action
-(IBAction)sideMenuButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Designing Functions
-(UIButton*)addButtonWithFrame:(CGRect)frame tag:(NSInteger)tag image:(UIImage*)image title:(NSString*)title color:(UIColor*)color font:(UIFont*)font
{
    UIButton *button=[UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:frame];
    [button setTag:tag];
    [button setTitle:title forState:UIControlStateNormal];
    [button setImage:image forState:UIControlStateNormal];
    [button setTitleColor:color forState:UIControlStateNormal];
    [button.titleLabel setFont:font];
    //[button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchDown];
    
    
    
    return button;
}



@end
